package com.driver.vro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.driver.vro.constants.MsgConstants;
import com.driver.vro.databinding.ActivitySignInBinding;
import com.driver.vro.utility.Utilities;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivitySignInBinding binding;
    public static Activity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        instance = this;
        setupScreen();
    }

    private void setupScreen() {
        clickListeners();
    }

    private void clickListeners() {
        binding.tvSignIn.setOnClickListener(this);
        binding.tvSignUp.setOnClickListener(this);
        binding.btnLetStart.setOnClickListener(this);
        binding.scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Utilities.hideSoftKeyboard(SignInActivity.this);
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == binding.tvSignUp) {
            Intent i = new Intent(SignInActivity.this, MainActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (view == binding.btnLetStart) {
            Utilities.hideSoftKeyboard(SignInActivity.this);
            if (formValidate())
                callSignInAPI();
        }
    }

    private void callSignInAPI() {
       /* Intent i = new Intent(SignInActivity.this, HomeActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/
    }

    private boolean formValidate() {

        if (binding.etMobileNumber.getText().toString().length() < 10) {
            Toast.makeText(this, MsgConstants.errorPhone, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }
}
