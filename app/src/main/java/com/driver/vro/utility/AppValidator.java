package com.driver.vro.utility;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.io.File;
import java.util.regex.Pattern;


public class AppValidator {

    public static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    //    public static final String NAME_REGEX = "^[_A-Za-z0-9-\\+]";
    public static final String NAME_REGEX = "^[A-Za-z0-9\\s]{1,}[\\.]{0,1}[A-Za-z0-9\\s]{0,}$";
    public static final String CHAR_REGEX = ".*[a-zA-Z]+.*";
    public static final String ONLY_CHAR_REGEX = "^[a-zA-Z ]*$";
    public static final String MOBILE_REGEX = "\\d{10}";
    public static final String MOBILE_REGEX_TEST = "\\d{10}";
    public static final String YEAR_REGEX = "\\d{4}";
    public static final String PINCODE_REGEX = "^([1-9])([0-9]){5}$";
    public static final String VEHICLE_REGEX = "^[A-Z]{2} [0-9]{2} [A-Z]{2} [0-9]{4}$";
    public static final String PASSWORD_REGEX = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=_])(?=\\S+$).{6,}$";//.{8,}
    public static final String[] IMAGE_EXTENSIONS = new String[]{"jpg", "jpeg", "png"};
    public static final String NUMBER_REGEX = "[0-9]+";

    public static boolean isValidEmail(Context context, EditText editText, String msg) {
        Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
        String EMAIL_REG = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        if (editText.getText().toString().trim().equals("")) {

            // editText.setError(msg);
//            AppUtilis.showToast(context, msg);
//            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().matches(EMAIL_REGEX)) {
            return true;
        } else {
//            AppUtilis.showToast(context, "Invalid Email");
            //editText.setError("Invalid Email");
//            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }
    }

    public static boolean isValidPassword(Context context, EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {

//            AppUtilis.showToast(context, msg);
            //editText.setError(msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().length() >= 6)
            return true;
        else {
//            AppUtilis.showToast(context, "Password should contain  min 6 char.");
            //editText.setError("Password should contain  min 6 char.");
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }
    }

//    public static boolean isValidMobile(String msg) {
//        if (editText.getText().toString().trim().equals("")) {
//
//            //editText.setError(msg);
////            AppUtilis.showToast(context, msg);
//            editText.addTextChangedListener(new RemoveErrorEditText(editText));
//            editText.requestFocus();
//            return false;
//        } else if (editText.getText().toString().matches(MOBILE_REGEX_TEST))
//            return true;
//        else {
////            editText.setError("invalid mobile");
////            AppUtilis.showToast(context, "invalid mobile");
//            editText.addTextChangedListener(new RemoveErrorEditText(editText));
//            editText.requestFocus();
//            return false;
//        }
//
//    }

    public static boolean isValidPincode(EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {

            editText.setError(msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().matches(PINCODE_REGEX))
            return true;
        else {
            editText.setError("invalid pincode");
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }
    }

    public static boolean isValidName(EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {

            editText.setError(msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().matches(CHAR_REGEX))
            return true;
        else {
            editText.setError("invalid name");
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }
    }

    public static boolean isValidAddress(EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {

            editText.setError(msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().matches(NAME_REGEX))
            return true;
        else {
            editText.setError("invalid address");
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }
    }

    public static boolean isOnlyChars(Context context, EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {

            //editText.setError(msg);
            AppUtilis.showToast(context, msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().matches(ONLY_CHAR_REGEX))
            return true;
        else {
            //editText.setError("invalid format");
            AppUtilis.showToast(context, "invalid name format");
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }

    }

    public static boolean isOnlyNumbers(Context context, EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {

            //editText.setError(msg);
//            AppUtilis.showToast(context, msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().matches(NUMBER_REGEX))
            return true;
        else {
            //editText.setError("invalid format");
//            AppUtilis.showToast(context, "invalid number format");
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }

    }

    public static boolean isValidVehicleNo(EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {

            editText.setError(msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        } else if (editText.getText().toString().matches(VEHICLE_REGEX))
            return true;
        else {
            editText.setError("invalid vehicle no.");
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }
    }


    public static boolean isValidImage(File file) {
        for (String extensions : IMAGE_EXTENSIONS) {
            if (file.getName().toLowerCase().endsWith(extensions))
                return true;
        }
        return false;
    }

    public static boolean isValidYear(String data) {
        if (data.matches(YEAR_REGEX))
            return true;
        else
            return false;
    }


    public static boolean isValid(Context context, EditText editText, String msg) {
        if (editText.getText().toString().trim().equals("")) {
            AppUtilis.showToast(context, msg);
            // editText.setError(msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            return false;
        }
        return true;
    }

    public static boolean isSamePassword(Context context, EditText editText, EditText editText2, String msg) {
        if (!editText.getText().toString().trim().equals(editText2.getText().toString().trim())) {
            AppUtilis.showToast(context, msg);
            //  editText.setError(msg);
            editText.addTextChangedListener(new RemoveErrorEditText(editText));
            editText.requestFocus();
            //    editText2.setError(msg);
            editText2.addTextChangedListener(new RemoveErrorEditText(editText));
            return false;
        } else
            return true;
    }


    public static class RemoveErrorEditText implements TextWatcher {

        private EditText editText;


        public RemoveErrorEditText(EditText edittext) {
            this.editText = edittext;

        }

        @Override
        public void afterTextChanged(Editable s) {

            editText.setError(null);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before,
                                  int count) {

        }

    }


}
