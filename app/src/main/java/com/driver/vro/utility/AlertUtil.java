package com.driver.vro.utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.driver.vro.R;
import com.google.android.material.snackbar.Snackbar;

/**
 * Created by Rakesh
 */

public class AlertUtil {
    private static ProgressDialog progressDialog;
    private static ProgressBar progressBar;

    public static void showSnackBarShort(Context context, View view, String msg) {
        if (context != null) {
            Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
    }

    public static void showSnackBarLong(Context context, View view, String msg, String buttonTitle, final View.OnClickListener onClickListener) {
        if (context != null) {
            Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
            snackbar.setAction(buttonTitle, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onClick(view);
                }
            });
            snackbar.show();
        }
    }

    public static void showSnackBarLong(Context context, View view, String msg) {
        if (context != null) {
            Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public static void showToastShort(Context context, String msg) {
        if (context != null)
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastLong(Context context, String msg) {
        if (context != null)
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showAlertDialog(Context context, String title, String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("Ok", null);
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    public static void showAlertDialogWithOK(Context context, String msg, DialogInterface.OnClickListener okListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(null);
        builder.setMessage(msg);
        builder.setPositiveButton("Ok", okListener);
        builder.show();
    }

    public static void showAlertDialog(Context context, String title, String msg, String button1, String button2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(button1, null);
        builder.setNegativeButton(button2, null);
        builder.show();
    }

    public static void showAlertDialog(Context context, String title, String msg, String button1, String button2, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(button1, okListener);
        builder.setNegativeButton(button2, cancelListener);
        builder.show();
    }

    public static void showAlertDialog(Context context, String title, String msg, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("Ok", okListener);
        builder.setNegativeButton("Cancel", cancelListener);
        builder.show();
    }

    public static void showProgressDialog(final Context context) {
        if (context != null) {
            if (progressDialog != null && progressDialog.isShowing()) {

            } else {
                progressDialog = new ProgressDialog(context);
                LayoutInflater inflater = LayoutInflater.from(context);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progress_dialog);
                TextView titleView = (TextView) progressDialog.findViewById(R.id.title);
                progressBar = (ProgressBar) progressDialog.findViewById(R.id.marker_progress);
                progressBar.getIndeterminateDrawable()
                        .setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN );
                titleView.setVisibility(View.GONE);
            }
        }
    }

    public static void setProgress(int progress) {
        if (progressDialog != null && progressBar != null && progressDialog.isShowing()) {
            progressBar.setProgress(progress);
        }
    }

    public static void showProgressDialogProgress(final Context context, final String title) {
        if (context != null) {
            if (progressDialog != null && progressDialog.isShowing()) {

            } else {
                progressDialog = new ProgressDialog(context);
                LayoutInflater inflater = LayoutInflater.from(context);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progress_dialog);
                TextView titleView = (TextView) progressDialog.findViewById(R.id.title);
                titleView.setText(title);
            }
        }
    }

    public static void setProgress(Context context, final int progress) {
        if (progressBar != null) {
            progressBar.setProgress(progress);
        }
    }


    public static void showProgressDialog(final Context context, String title) {
        if (context != null) {
            if (progressDialog != null && progressDialog.isShowing()) {

            } else {
                progressDialog = new ProgressDialog(context);
                LayoutInflater inflater = LayoutInflater.from(context);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.show();
                progressDialog.setContentView(R.layout.progress_dialog);
                TextView titleView = (TextView) progressDialog.findViewById(R.id.title);
                titleView.setVisibility(View.VISIBLE);
                titleView.setText(title);
            }
        }
    }

    public static void hideProgressDialog() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }catch (Exception ex)
        {

        }
    }

}
