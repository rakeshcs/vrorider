package com.driver.vro.utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.driver.vro.constants.ServerConstants;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class AppUtilis {

    public static Uri mImageUri;
    public static ProgressDialog pd;
//    static GetDob getDob;


    /**
     * To redirect from one activity to another activity
     *
     * @param context
     * @param act
     */
    public static void goToActivity(Context context, Class<?> act) {
        Intent i = new Intent(context, act);
        context.startActivity(i);
    }

    public static String getLoggedInUserId(Context context) {
        String userId = SharedPrefManager.getInstance(context).getString(ServerConstants.USER_ID, "");
        return userId;
    }

    public static void loadImageViewUrl(String url, ImageView imageView, int placeHolder) {
        if(url!=null && url.length()>0)
            Picasso.get().load(url).placeholder(placeHolder).error(placeHolder).into(imageView);
    }

    public static void loadImageViewUrlNoCache(String url, ImageView imageView, int placeHolder) {
        if(url!=null && url.length()>0) {
            Picasso.get().load(url)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .placeholder(placeHolder).error(placeHolder).into(imageView);
        }
        else
        {
            Picasso.get().load(placeHolder)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .placeholder(placeHolder).error(placeHolder).into(imageView);
        }
    }

    public static void clearAllgoToActivity(Context context, Class<?> act) {
        Intent i = new Intent(context, act);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    public static String getColoredSpanned(String text, String color) {
        String input = "<b><font color=" + color + ">" + text + "</font></b>";
        return input;
    }


    /**
     * To format the given date in a specific format
     *
     * @param year  Year
     * @param month Month
     * @param day   Day
     * @return String date
     */
    public static String formatDate(int year, int month, int day) {

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(0);
        cal.set(year, month, day);
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        return sdf.format(date);
    }

    /**
     * To show error message
     *
     * @param string  Tag for the error
     * @param message Message for the error
     */
    public static void showErrorLog(String string, String message) {
        Log.d(string, message);

    }

    /**
     * To check that email is valid or not.
     *
     * @param email Email String on which pattern will check.
     * @return True if entered email is valid and False if entered email is
     * invalid
     */
    public static boolean isEmailValid(final CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * To get text from EditText
     *
     * @param edtTxt EditText
     * @return String from EditText
     */
    public static String getText(final EditText edtTxt) {
        return edtTxt.getText().toString().trim();
    }

    public static String getText(final TextView Txt) {
        return Txt.getText().toString().trim();
    }

    public static void getWaitDialog(final Context context) {
        pd = new ProgressDialog(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        pd.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        pd.show();
//        pd.setContentView(inflater.inflate(R.layout.layout_dialog, null));
    }


    public static ProgressDialog getReturnDialog(final Context context) {
        ProgressDialog pd = new ProgressDialog(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        pd.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        pd.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        pd.setCancelable(false);
        pd.show();
        //pd.setMessage(context.getResources().context.getString(R.string.please_wait));
        // pd.setContentView(inflater.inflate(R.layout.layout_dialog, null));
        return pd;
    }


    public static void dismissProgressDialog() {
        if (pd != null && pd.isShowing()) {
            pd.dismiss();
        }
    }

    public static String getString(final Context context, final int resId) {
        return ((Activity) context).getResources().getString(resId);
    }


    public static boolean isNetworkAvailable(final Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        // should check null because in air plan mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    public static void showToast(final Context context, final String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastLong(final Context context, final String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static String setTextDouble(String text) {
        if (text != null && !text.equals("null") && !text.equals("")) {
            return String.format("%.2f", Double.parseDouble(text));
        }
        return "0.0";
    }

    public static String decimalTwoPlaces(String amount) {
        String decimalValue = "";
        if (amount != null & !amount.equals("")) {
            decimalValue = String.format("%.2f", Double.parseDouble(amount));
        }
        return decimalValue;
    }

    public static String arabicToNumaric(String amount) {
        String decimalValue = "";
        if (amount != null & !amount.equals("")) {
            decimalValue = amount.toString().replaceAll("٠", "0")
                    .replaceAll("٫", ".")
                    .replaceAll("١", "1")
                    .replaceAll("٢", "2")
                    .replaceAll("٣", "3")
                    .replaceAll("٤", "4")
                    .replaceAll("٥", "5")
                    .replaceAll("٦", "6")
                    .replaceAll("٧", "7")
                    .replaceAll("٨", "8")
                    .replaceAll("٩", "9");
        }
        return decimalValue;
    }

    @SuppressLint("NewApi")
    public static int[] getScreenSpec(Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return new int[]{width, height};
    }


    public static Bitmap grabImage(final Context context, final Uri mImageUri) {
        context.getContentResolver().notifyChange(mImageUri, null);
        ContentResolver cr = context.getContentResolver();
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(cr,
                    mImageUri);
        } catch (Exception e) {
            //   Toast.makeText(context, context.getResources().context.getString(R.string.failed_load), Toast.LENGTH_SHORT).show();
        }
        return bitmap;
    }


    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    public static void showSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        } catch (Exception e) {

        }
    }


    public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
        if (bitmap != null) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            bitmap.compress(CompressFormat.JPEG, 70, outputStream);
            return outputStream.toByteArray();
        } else {
            return null;
        }
    }

    public static String encodeImage(byte[] imageByteArray) {
        String encodeString = null;
        if (imageByteArray != null) {
            encodeString = Base64
                    .encodeToString(imageByteArray, Base64.DEFAULT);
        }
        return encodeString;
    }

    public static float dipToPixels(Context context, float f) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, f,
                metrics);
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier(
                "status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        Log.e("HEIGHT", result + "");
        return result;
    }


    public static String getCountryNameFromLocale() {
        Locale defaultLocale = Locale.getDefault();
        String countryName = defaultLocale.getDisplayCountry();
        Log.e("COUNTRY", countryName);
        return countryName;
    }

    public static void shareData(Context context, String text, Bitmap path) {
        try {
            String path1 = MediaStore.Images.Media.insertImage(context.getContentResolver(), path, "", null);
            Uri screenshotUri = Uri.parse(path1);
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, text);
            shareIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
            shareIntent.setType("image/*");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            context.startActivity(Intent.createChooser(shareIntent, "Share images..."));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void sendIntent(Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, "");
        intent.putExtra(Intent.EXTRA_TEXT, "");
        Intent mailer = Intent.createChooser(intent, null);
        context.startActivity(mailer);
    }

    public static void shareApplication(Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.techugo.prozata&hl=en");
        context.startActivity(Intent.createChooser(intent, "Share with"));
    }


    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight, InputStream is) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeStream(is, null, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }


    public static String getRealPathFromURI(Context context, Uri contentUri) {
        try {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = context.getContentResolver().query(contentUri,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            return cursor.getString(columnIndex);
        } catch (Exception e) {
            return contentUri.getPath();
        }

    }

    public static String getDateFromTimeStamp(long timestamp) {
        try {
            DateFormat df = DateFormat.getTimeInstance();
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            String gmtTime = df.format(new Date());

            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
            String date = dateFormatGmt.format(new Date());
            return date;
        } catch (Exception e) {
        }
        return "";
    }

    public static String getDateFromTimeStampComment(long timestamp) {
        try {
//            DateFormat df = DateFormat.getTimeInstance();
//            df.setTimeZone(TimeZone.getTimeZone("UTC"));
//            String gmtTime = df.format(new Date());
            Calendar aGMTCalendar = Calendar.getInstance();
            Date phoneTimeUTC = aGMTCalendar.getTime();
            SimpleDateFormat dateFormatGmt = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
            String date = dateFormatGmt.format(phoneTimeUTC);
            return date;
        } catch (Exception e) {
        }
        return "";
    }


    public static void showAlert(Context context, String title, String msg) {
        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(
                context);

        // set title
        alertDialogBuilder.setTitle(title);

        // set dialog message
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if context button is clicked, close
                        // current activity
                        dialog.dismiss();
                    }


                });

        // create alert dialog
        androidx.appcompat.app.AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

   /* public static LatLngBounds getLatLngBounds(LatLng center) {
        double radiusDegrees = 1.0;
        LatLng northEast = new LatLng(center.latitude + radiusDegrees, center.longitude + radiusDegrees);
        LatLng southWest = new LatLng(center.latitude - radiusDegrees, center.longitude - radiusDegrees);
        LatLngBounds bounds = LatLngBounds.builder()
                .include(northEast)
                .include(southWest)
                .build();

        return bounds;
    }*/


    public static Bitmap changeSize(BitmapDrawable bitmapDrawable, int width, int height) {
        Bitmap b = bitmapDrawable.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        return smallMarker;
    }


    public static String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";


        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String time = hours + ":" + minutes + " " + timeSet;
        return time;

    }

    public static String convertInputStreamToString(InputStream inputStream) throws IOException {
        StringBuilder result = null;
        try {
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            result = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                result.append(line).append('\n');
            }
            return result.toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public static void showStatus(TextView textView, ListView listView, String value, boolean isShow) {
        if (textView != null && listView != null && value != null) {
            if (isShow) {
                textView.setVisibility(View.VISIBLE);
                textView.setText(value);
                listView.setVisibility(View.GONE);
            } else {
                textView.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);
            }
        }
    }


    public static String formatDateTime(String time, String inputPattern, String outputPattern) {

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    public static String getActivityName(Activity activity) {

        if (activity != null) {
            return activity.getClass().getSimpleName();
        } else {
            return "";
        }

    }

//    public static void openDatePicker(Context context, final GetDob listeners) {
//        // Get Current Date
//        final Calendar c = Calendar.getInstance();
//        int mYear = c.get(Calendar.YEAR);
//        int mMonth = c.get(Calendar.MONTH);
//        int mDay = c.get(Calendar.DAY_OF_MONTH);
//        //launch datepicker modal
//        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
//                new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                        listeners.getDobSuccess(dayOfMonth, monthOfYear + 1, year);
//                    }
//                }, mYear, mMonth, mDay);
//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//        datePickerDialog.show();
//
//    }

    public static String getDateFromMillis(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.ENGLISH);
        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }


    public static String getCurrentDate() {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        String date = dateFormatGmt.format(new Date());

        return date;


    }


    public static String getTwoMonthBeforeDate(int month) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -month);
        c.add(Calendar.DAY_OF_MONTH, +1);

        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        String m, d;

        if (mMonth < 10) {
            m = "0" + mMonth + "";
        } else {
            m = mMonth + "";
        }

        if (mDay < 10) {
            d = "0" + mDay + "";
        } else {
            d = mDay + "";
        }


        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(mYear + "-" + m + "-" + d);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;

    }


    public static String getFromDate(int days) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, +1);
        c.add(Calendar.DAY_OF_MONTH, -days);

        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        String m, d;

        if (mMonth < 10) {
            m = "0" + mMonth + "";
        } else {
            m = mMonth + "";
        }

        if (mDay < 10) {
            d = "0" + mDay + "";
        } else {
            d = mDay + "";
        }


        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(mYear + "-" + m + "-" + d);
            date.setHours(12);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;


    }


    public static void clearNotifications(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }


//    public static void setProfileImagePicasso(Context context, ImageView view, String path) {
//        if (path.length() != 0) {
//            Picasso.with(context)
//                    .load(path)
//                    .placeholder(R.drawable.user)
//                    .fit()
//                    .centerCrop()
//                    .into(view);
//        } else {
//            Picasso.with(context)
//                    .load(R.drawable.user)
//                    .placeholder(R.drawable.user)
//                    .fit()
//                    .centerCrop()
//                    .into(view);
//        }
//    }

//    public static void setMovieImagePicasso(Context context, ImageView view, String path) {
//        if (path.length() != 0) {
//            Picasso.with(context)
//                    .load(path)
//                    .placeholder(R.drawable.movie_default)
//                    .fit()
//                    .centerCrop()
//                    .into(view);
//        } else {
//            Picasso.with(context)
//                    .load(R.drawable.movie_default)
//                    .placeholder(R.drawable.movie_default)
//                    .fit()
//                    .centerCrop()
//                    .into(view);
//        }
//    }


    public static String decodeBase64String(String encoded) {
        byte[] dataDec = Base64.decode(encoded, Base64.DEFAULT);
        String decodedString = "";
        try {

            decodedString = new String(dataDec, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();

        } finally {

            return decodedString;
        }
    }


    public static String extractYTId(String ytUrl) {
        String vId = null;
        Pattern pattern = Pattern.compile(
                "http(?:s?):\\/\\/(?:www\\.)?youtu(?:be\\.com\\/watch\\?v=|\\.be\\/)([\\w\\-\\_]*)(&(amp;)?\u200C\u200B[\\w\\?\u200C\u200B=]*)?",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(ytUrl);
        if (matcher.matches()) {
            vId = matcher.group(1);
        }
        return vId;
    }

    public static boolean checkJson(String str) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (jsonObject != null) {
            return true;
        } else {
            return false;
        }
    }

    //BLUR THE IMAGE
    public static Bitmap scriptIntrinsicBlur(Bitmap src, float r, Activity activity) {
        //Radius range (0 < r <= 25)
        if (r <= 0) {
            r = 0.1f;
        } else if (r > 25) {
            r = 25.0f;
        }
        Bitmap bitmap = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
        RenderScript renderScript = RenderScript.create(activity);
        Allocation blurInput = Allocation.createFromBitmap(renderScript, src);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);
        ScriptIntrinsicBlur blur = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            blur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
            blur.setInput(blurInput);
            blur.setRadius(r);
            blur.forEach(blurOutput);
        }

        blurOutput.copyTo(bitmap);
        renderScript.destroy();
        return bitmap;
    }


    //take ScreenShot of screen and BLUR the Image
    public static Bitmap blurScreenShot(float r, Activity activity, View view) {
        // View view = activity.getWindow().getDecorView().getRootView();
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
        bitmap = scriptIntrinsicBlur(bitmap, r, activity);
        return bitmap;
    }

    public static void openYouTubeHandle(Context context, String pageName) {
        // context.getPackageManager().getLaunchIntentForPackage("com.google.android.youtube");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setPackage("com.google.android.youtube");
        intent.setData(Uri.parse(pageName));
        context.startActivity(intent);
    }



}
