package com.driver.vro.utility;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Naresh on 10/7/2016.
 */

public class Utilities {
    public static String LOG_TAG = "eventLog";
    public static String MY_LOG = "eventLog";

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    //    public static BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
//        Canvas canvas = new Canvas();
//        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//        canvas.setBitmap(bitmap);
//        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
//        drawable.draw(canvas);
//        return BitmapDescriptorFactory.fromBitmap(bitmap);
//    }

    public static String formatDateTime(String time, String inputPattern, String outputPattern) {

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
        //  outputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void setStatusBarGradient(Activity activity, int gradientId) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = activity.getWindow();
            Drawable background = activity.getResources().getDrawable(gradientId);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setNavigationBarColor(activity.getResources().getColor(android.R.color.transparent));
            window.setBackgroundDrawable(background);
        }
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    // for logging out the user
//    public static void logOut(Activity context) {
//        SharedPrefManager.getInstance(context).removeData(PrefConstants.USER_LOGGED_IN);
//        SharedPrefManager.getInstance(context).removeData(PrefConstants.USER_ID);
//        SharedPrefManager.getInstance(context).removeData(PrefConstants.NAME);
//        SharedPrefManager.getInstance(context).removeData(PrefConstants.IMAGE);
//        SharedPrefManager.getInstance(context).removeData(PrefConstants.EMAIL);
//        SharedPrefManager.getInstance(context).removeData(PrefConstants.MOBILE);
//
//        Intent intent = new Intent(context, DoctorLoginActivity.class);
//        context.startActivity(intent);
//        context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//        context.finish();
//        if (HomeActivity.instance != null)
//            HomeActivity.instance.finish();
//    }


    public static long getDifferenceTime(String date) {

        //HH converts hour in 24 hours format (0-23), day calculatio
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateStart = format.format(new Date());

        Date d1 = null;
        Date d2 = null;
        long diff = 0;
        try {
            d1 = format.parse(dateStart);
            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            d2 = newFormat.parse(date);

            //in milliseconds
            diff = d2.getTime() - d1.getTime();

       /*     long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60  60  1000) % 24;
            long diffDays = diff / (24  60  60 * 1000);*/

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return diff;
    }

    public static String changeDateFormat(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat reqDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String newDate = "";
        try {
            Date myDate = simpleDateFormat.parse(date);
            newDate = reqDateFormat.format(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }


    public static String changeTimeFormat(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat reqDateFormat = new SimpleDateFormat("hh:mm aa");
        String newDate = "";
        try {
            Date myDate = simpleDateFormat.parse(date);
            newDate = reqDateFormat.format(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public static String reverseTimeFormat(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat reqDateFormat = new SimpleDateFormat("HH:mm:ss");
//        reqDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String newDate = "";
        try {
            Date myDate = simpleDateFormat.parse(date);
            newDate = reqDateFormat.format(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public static String convertUTCToLocal(String time) {
        String year = "" + time.charAt(0) + time.charAt(1) + time.charAt(2) + time.charAt(3);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String newDateStr = "";
        try {
            Date myDate = simpleDateFormat.parse(time);
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM, hh:mm a");
            SimpleDateFormat postFormaterYear = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
            if (year.equals(String.valueOf(Calendar.getInstance().get(Calendar.YEAR))))
                newDateStr = postFormater.format(myDate);
            else newDateStr = postFormaterYear.format(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDateStr;
    }

    public static String convertUTCtoLocal(String time) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String newDateStr = "";
        try {
            Date myDate = simpleDateFormat.parse(time);
            SimpleDateFormat postFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            newDateStr = postFormater.format(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDateStr;
    }

    public static String convertLocalToUTC(String time) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        df.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = df.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            df.setTimeZone(TimeZone.getTimeZone("UTC"));
            String formattedDate = df.format(date);
            return formattedDate;
        } else return null;
    }

    public static String convertDateTimeFormat(String time) {
        String year = "" + time.charAt(0) + time.charAt(1) + time.charAt(2) + time.charAt(3);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
        String newDateStr = "";
        try {
            Date myDate = simpleDateFormat.parse(time);
            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMM, hh:mm a");
            SimpleDateFormat postFormaterYear = new SimpleDateFormat("dd MMM yyyy, hh:mm a");
            if (year.equals(String.valueOf(Calendar.getInstance().get(Calendar.YEAR))))
                newDateStr = postFormater.format(myDate);
            else newDateStr = postFormaterYear.format(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDateStr;
    }

    public static String getImageAsString(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        byte[] encoded = Base64.encode(imageBytes, 0);
        String encodedString = new String(encoded);
        return encodedString;
    }

    public static Bitmap captureScreenshot(Activity activity) {
        View v1 = activity.getWindow().getDecorView().getRootView();
        v1.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        return bitmap;
    }

    public static String getActivityName(Activity activity) {
        if (activity != null) {
            return activity.getClass().getSimpleName();
        } else {
            return "";
        }
    }

//    public static String getCountryZipCode(Context context) {
//        String CountryID = "";
//        String CountryZipCode = "";
//
//        TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
//        //getNetworkCountryIso
//        CountryID = manager.getSimCountryIso().toUpperCase();
//        String[] rl = context.getResources().getStringArray(R.array.CountryCode);
//        for (int i = 0; i < rl.length; i++) {
//            String[] g = rl[i].split(",");
//            if (g[1].trim().equals(CountryID.trim())) {
//                CountryZipCode = g[0];
//                break;
//            }
//        }
//        return "+" + CountryZipCode;
//    }

//    public static Bitmap scriptIntrinsicBlur(Bitmap src, float r, Activity activity) {
//        //Radius range (0 < r <= 25)
//        if (r <= 0) {
//            r = 0.1f;
//        } else if (r > 25) {
//            r = 25.0f;
//        }
//        Bitmap bitmap = Bitmap.createBitmap(src.getWidth(), src.getHeight(), Bitmap.Config.ARGB_8888);
//        RenderScript renderScript = RenderScript.create(activity);
//        Allocation blurInput = Allocation.createFromBitmap(renderScript, src);
//        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);
//        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
//        blur.setInput(blurInput);
//        blur.setRadius(r);
//        blur.forEach(blurOutput);
//        blurOutput.copyTo(bitmap);
//        renderScript.destroy();
//        return bitmap;
//    }

    public static boolean validateFirstName(String firstName) {
        return firstName.matches("[A-Z][a-zA-Z]*");
    } // end method validateFirstName

    // validate last name
    public static boolean validateLastName(String lastName) {
        return lastName.matches("[a-zA-z]+([ '-][a-zA-Z]+)*");
    }

    public static void alertBoxSingleBtn(Context context, String msg) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(msg)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
// startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static boolean isValidEmail(CharSequence text) {
        if (text == null)
            return false;
        else
            return Patterns.EMAIL_ADDRESS.matcher(text).matches();
    }

    public static boolean isValidEmailMobile(EditText editText) {
        if (editText.getText().toString().trim().isEmpty()) {
            return false;
        } else if (editText.getText().toString().trim().matches(Patterns.EMAIL_ADDRESS.toString())) {
            return true;
        } else if (editText.getText().toString().trim().matches(Patterns.PHONE.toString())) {
            return true;
        } else {
            return false;
        }
    }

    public static void mLog(String msg) {
        Log.e(MY_LOG, msg);
    }

    public static void hideKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                    .getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final static boolean isValidPhone(CharSequence target) {
        if (target == null) {
            return false;
        } else {

            return Patterns.PHONE.matcher(target)
                    .matches() && (target.length() >= 10 && target.length() <= 20);
        }
    }

    public static boolean isValidMobile(String phone) {
        String regex = "[0-9]+";
        StringBuilder bul = new StringBuilder(phone);

        if (phone.contains("(")) {
            bul.deleteCharAt(0);
        }
        if (phone.contains(")")) {
            bul.deleteCharAt(4);
        }
        if (phone.contains(" ")) {
            if (String.valueOf(bul.charAt(0)).equalsIgnoreCase("1")) {
                bul.deleteCharAt(1);
            } else {
                bul.deleteCharAt(3);
            }

        }

        if (bul.substring(0, 3).equalsIgnoreCase("000")) {
            return false;
        }
        return bul.toString().matches(regex);
        //return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static String getDeviceID(Activity activity) {
        return Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        if (activity.getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public static boolean isLastActivityInTheStack(Context context, String className) {
        ActivityManager mngr = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);

        List<ActivityManager.RunningTaskInfo> taskList = mngr.getRunningTasks(10);

        if (taskList.get(0).numActivities == 1 &&
                taskList.get(0).topActivity.getClassName().equals(className))

        {
            return true;
        } else {

        }
        for (ActivityManager.RunningTaskInfo info : taskList) {
            //Log.i(Utilities.MY_LOG, "T  info.getClass().getName():" + info.getClass().getName());

        }
        return false;
    }

    public static void pickDate(final Context context, final TextView editText) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (validateNextDate(context, dayOfMonth, monthOfYear, year)) {
                    String dd, yy, mm;
                    if ((monthOfYear + 1) < 10)
                        mm = "0" + (monthOfYear + 1);
                    else
                        mm = String.valueOf(monthOfYear + 1);
                    if (dayOfMonth < 10)
                        dd = "0" + dayOfMonth;
                    else
                        dd = dayOfMonth + "";
                    if (editText != null)
                        editText.setText(year + "-" + mm + "-" + dd);
                }
            }
        }, mYear, mMonth, mDay);

        dpd.getDatePicker().setMaxDate(c.getTimeInMillis());
        dpd.show();

    }

    public static void pickDate2(final Context context, final TextView editText) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (validatePastDate(context, dayOfMonth, monthOfYear, year)) {
                    String dd, yy, mm;
                    if ((monthOfYear + 1) < 10)
                        mm = "0" + (monthOfYear + 1);
                    else
                        mm = String.valueOf(monthOfYear + 1);
                    if (dayOfMonth < 10)
                        dd = "0" + dayOfMonth;
                    else
                        dd = dayOfMonth + "";
                    if (editText != null)
                        editText.setText(year + "-" + mm + "-" + dd);
                }
            }
        }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate(c.getTimeInMillis());
        dpd.show();

    }

    public static void pickDateValidatePast(final Context context, final DateSelector dateSelector) {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {


            @Override
            public void onDateSet(DatePicker view, int year,
                                  int monthOfYear, int dayOfMonth) {
                if (validatePastDate(context, dayOfMonth, monthOfYear, year)) {
                    String dd, yy, mm;
                    if ((monthOfYear + 1) < 10)
                        mm = "0" + (monthOfYear + 1);
                    else
                        mm = String.valueOf(monthOfYear + 1);
                    if (dayOfMonth < 10)
                        dd = "0" + dayOfMonth;
                    else
                        dd = dayOfMonth + "";
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("EEEE, dd MMM, yyyy");
                        Date selDate = dateFormat.parse(year + "-" + mm + "-" + dd);
                        dateSelector.onDateSelected(dateFormatter.format(selDate));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, mYear, mMonth, mDay);
        dpd.getDatePicker().setMinDate(c.getTimeInMillis());
        dpd.show();

    }

    public static boolean validatePastDate(Context mContext, int day, int month, int year) {
        final Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH) + 1;
        int currentDay = c.get(Calendar.DAY_OF_MONTH);
        if (day < currentDay /*&& year == currentYear && month == currentMonth*/) {
            Toast.makeText(mContext, "Please select a valid date", Toast.LENGTH_LONG).show();
            return false;
        }/* else if (month < currentMonth && year == currentYear) {
            Toast.makeText(mContext, "Please select valid month", Toast.LENGTH_LONG).show();
            return false;
        } else if (year < currentYear) {
            Toast.makeText(mContext, "Please select valid year", Toast.LENGTH_LONG).show();
            return false;
        }*/

        return true;
    }

    public static boolean validateNextDate(Context mContext, int day, int month, int year) {
        final Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR);
        int currentMonth = c.get(Calendar.MONTH) + 1;
        int currentDay = c.get(Calendar.DAY_OF_MONTH);
        if (day > currentDay & year == currentYear & month == currentMonth) {
            Toast.makeText(mContext, "Please select a valid date", Toast.LENGTH_LONG).show();
            return false;
        } else if (month > currentMonth & year == currentYear) {
            Toast.makeText(mContext, "Please select valid month", Toast.LENGTH_LONG).show();
            return false;
        } else if (year > currentYear) {
            Toast.makeText(mContext, "Please select valid year", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    public static void pickTime(Context context, final TextView editText) {

        final Calendar calendar = Calendar.getInstance();
        int mHour = calendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = calendar.get(Calendar.MINUTE);
        TimePickerDialog tpd = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String hrs, mins;

                        if (hourOfDay < 10) {
                            hrs = ("0" + hourOfDay);
                            //etHrs.setText("0" + hourOfDay + "");
                        } else {
                            hrs = String.valueOf(hourOfDay);
                            //etHrs.setText(hourOfDay + "");
                        }

                        if (minute < 10) {
                            mins = String.valueOf("0" + minute);
                            //etMins.setText("0" + minute + "");
                        } else {
                            mins = String.valueOf(minute);
                            //etMins.setText(minute + "");
                        }
                        if (editText != null)
                            editText.setText(hrs + ":" + mins);
                    }
                }, mHour, mMinute, false);

        tpd.show();

    }


   /* public static void pickTimeWithAmPm(Context context, final TimeSelector timeSelector) {

        final Calendar calendar = Calendar.getInstance();
        int mHour = calendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = calendar.get(Calendar.MINUTE);
        CustomTimePickerDialog tpd = new CustomTimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        String hrs, mins;

                        String amPm = "AM";

                        if (hourOfDay > 11) {
                            // If the hour is greater than or equal to 12
                            // Then the current AM PM status is PM
                            amPm = "PM";
                        }
                        // Initialize a new variable to hold 12 hour format hour value
                        int hour_of_12_hour_format;

                        if (hourOfDay > 12) {

                            // If the hour is greater than or equal to 12
                            // Then we subtract 12 from the hour to make it 12 hour format time
                            hour_of_12_hour_format = hourOfDay - 12;
                        } else if (hourOfDay == 0) {

                            // If the hour is greater than or equal to 12
                            // Then we subtract 12 from the hour to make it 12 hour format time
                            hour_of_12_hour_format = 12;
                        } else {
                            hour_of_12_hour_format = hourOfDay;
                        }

                        if (hour_of_12_hour_format < 10) {
                            hrs = ("0" + hour_of_12_hour_format);
                            //etHrs.setText("0" + hourOfDay + "");
                        } else {
                            hrs = String.valueOf(hour_of_12_hour_format);
                            //etHrs.setText(hourOfDay + "");
                        }

                        if (minute < 10) {
                            mins = String.valueOf("0" + minute);
                            //etMins.setText("0" + minute + "");
                        } else {
                            mins = String.valueOf(minute);
                            //etMins.setText(minute + "");
                        }
                        // if (editText != null)
                        //editText.setText(hrs + ":" + mins + " "+amPm);
                        timeSelector.onTimeSelected(hrs + ":" + mins + " " + amPm);
                    }
                }, mHour, mMinute, false);

        tpd.show();

    }*/

    public interface TimeSelector {
        void onTimeSelected(String selectedTime);
    }

    public interface DateSelector {
        void onDateSelected(String selectedDate);
    }

    public static boolean validateTime(String time, String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");

            Date now = new Date(System.currentTimeMillis());
            int result = now.compareTo(dateFormatter.parse(date));
            if (result < 0) {
                return true;
            }

            Date EndTime = dateFormat.parse(time);

            Date CurrentTime = dateFormat.parse(dateFormat.format(new Date()));

            if (CurrentTime.after(EndTime)) {
                System.out.println("timeeee end ");
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static int getAge(int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        return a;
    }


    public static Bitmap getBitMapFromImageURl(String imagepath, Activity activity) {

        Bitmap bitmapFromMapActivity = null;
        Bitmap bitmapImage = null;
        try {

            File file = new File(imagepath);
            // We need to recyle unused bitmaps
            if (bitmapImage != null) {
                bitmapImage.recycle();
            }
            bitmapImage = reduceImageSize(file, activity);
            int exifOrientation = 0;
            try {
                ExifInterface exif = new ExifInterface(imagepath);
                exifOrientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            } catch (IOException e) {
                e.printStackTrace();
            }

            int rotate = 0;

            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
            }

            if (rotate != 0) {
                int w = bitmapImage.getWidth();
                int h = bitmapImage.getHeight();

                // Setting pre rotate
                Matrix mtx = new Matrix();
                mtx.preRotate(rotate);

                // Rotating Bitmap & convert to ARGB_8888, required by
                // tess

                Bitmap myBitmap = Bitmap.createBitmap(bitmapImage, 0, 0, w, h,
                        mtx, false);
                bitmapFromMapActivity = myBitmap;
            } else {
                int SCALED_PHOTO_WIDTH = 150;
                int SCALED_PHOTO_HIGHT = 200;
                Bitmap myBitmap = Bitmap.createScaledBitmap(bitmapImage,
                        SCALED_PHOTO_WIDTH, SCALED_PHOTO_HIGHT, true);
                bitmapFromMapActivity = myBitmap;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return bitmapFromMapActivity;

    }

    public static Bitmap reduceImageSize(File f, Context context) {

        Bitmap m = null;
        try {

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            final int REQUIRED_SIZE = 150;

            int width_tmp = o.outWidth, height_tmp = o.outHeight;

            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            o2.inPreferredConfig = Bitmap.Config.ARGB_8888;
            m = BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {
            // Toast.makeText(context,
            // "Image File not found in your phone. Please select another image.",
            // Toast.LENGTH_LONG).show();
        } catch (Exception e) {
        }
        return m;
    }


    public static boolean checkEmailId(String emailId) {
        return Patterns.EMAIL_ADDRESS.matcher(emailId).matches();
    }

    public static String getGender(String gender) {
        String gen;
        if (gender.equals("M")) {
            gen = "Male";
        } else {
            gen = "Female";
        }

        return gen;
    }

    public static boolean isValidJson(String responseStr) {

        try {
            new JSONObject(responseStr);
            return true;
        } catch (JSONException ex) {
            try {
                new JSONArray(responseStr);
                return true;
            } catch (JSONException ex1) {
                return false;
            }
        }
    }

    public int convertDpToPixel(Context context, int dp) {
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                r.getDisplayMetrics()
        );
        return px;
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }


    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static void SendEmail(Activity context, String to, String subject) {
        String[] TO = {to};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        PackageManager manager = context.getPackageManager();
        List infos = manager.queryIntentActivities(emailIntent, 0);
        if (infos.size() > 0) {
            context.startActivity(emailIntent);
        } else {
            AlertUtil.showToastShort(context, "No Email application installed on phone");
        }
    }

    public static String getTimeStamp() {

        long timestamp = (System.currentTimeMillis() / 1000L);
        String tsTemp = "" + timestamp;
        return "" + tsTemp;

    }


    public static long getDateDiffInDays(Date date1, Date date2) {

        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        return days;
    }

    public static long getDateDiffInHours(Date date1, Date date2) {

        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;

        return hours;
    }

    public static void CopyStream(InputStream is, OutputStream os) {
        final int buffer_size = 1024;
        try {
            byte[] bytes = new byte[buffer_size];
            for (; ; ) {
                int count = is.read(bytes, 0, buffer_size);
                if (count == -1)
                    break;
                os.write(bytes, 0, count);
            }
        } catch (Exception ex) {
        }
    }

    public static String streamToString(InputStream is) throws IOException {
        String str = "";

        if (is != null) {
            StringBuilder sb = new StringBuilder();
            String line;

            try {
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(is));

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                reader.close();
            } finally {
                is.close();
            }

            str = sb.toString();
        }

        return str;
    }

    public static long getDateDiffInMinutes(Date date1, Date date2) {

        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        return minutes;
    }

    public static long getDateDiffInSeconds(Date date1, Date date2) {

        long diff = date1.getTime() - date2.getTime();
        long seconds = diff / 1000;
        return seconds;
    }

    public static long getDateDiffInMilliSeconds(Date date1, Date date2) {

        long diff = date1.getTime() - date2.getTime();
        return diff;
    }

    public static void showLog(String tag, String logMsg) {
        Log.d(LOG_TAG, tag + "==>" + logMsg);
    }

//    public static void startActivity(Context context, Class<?> cls) {
//        Intent intent = new Intent(context, cls);
//        context.startActivity(intent);
////        ((Activity) context).overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//    }

    public static Bitmap decodeSampledBitmapFromFile(String pic_Path, int reqWidth, int reqHeight) {
        try {
            File f = new File(pic_Path);
            Matrix mat = getOrientatationFromFile(f.getPath());// postRotate(angle);
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(pic_Path, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth,
                    reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            // return BitmapFactory.decodeFile(pic_Path, options);
            Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(f),
                    null, options);
            Bitmap correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                    bmp.getHeight(), mat, true);
            return correctBmp;
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);// Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);// Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    static Matrix getOrientatationFromFile(String pic_Path) {
        Matrix mat = new Matrix();
        try {

            ExifInterface exif = new ExifInterface(pic_Path);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            int angle = 0;

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270;
            }

            mat.postRotate(angle);
            return mat;
        } catch (Exception e) {
            return mat;
        }
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static class DateTimeDifference {
        private String days = "";
        private String hours = "";
        private String mins = "";
        private String secs = "";

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getMins() {
            return mins;
        }

        public void setMins(String mins) {
            this.mins = mins;
        }

        public String getSecs() {
            return secs;
        }

        public void setSecs(String secs) {
            this.secs = secs;
        }
    }


    public static Bitmap getBitmapFromReturnedImage(Context context, Uri selectedImage, int reqWidth, int reqHeight) throws IOException {

        InputStream inputStream = context.getContentResolver().openInputStream(selectedImage);

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(inputStream, null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // close the input stream
        inputStream.close();

        // reopen the input stream
        inputStream = context.getContentResolver().openInputStream(selectedImage);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeStream(inputStream, null, options);
    }

    public static Bitmap rotateBitmap(String filePath, Bitmap bitmap) {
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }

    public static Bitmap scaleImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > 300 || rotatedHeight > 300) {
            float widthRatio = ((float) rotatedWidth) / ((float) 300);
            float heightRatio = ((float) rotatedHeight) / ((float) 300);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

        /*
         * if the orientation is not 0 (or -1, which means we don't know), we
         * have to do a rotation.
         */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        String type = context.getContentResolver().getType(photoUri);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (type.equals("image/png")) {
            srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
            srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        }
        byte[] bMapArray = baos.toByteArray();
        baos.close();
        return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
    }

    public static int getOrientation(Context context, Uri photoUri) {
        /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }


    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Images.Media._ID},
                MediaStore.Images.Media.DATA + "=? ",
                new String[]{filePath}, null);
        if (cursor != null && cursor.moveToFirst()) {
            int id = cursor.getInt(cursor
                    .getColumnIndex(MediaStore.MediaColumns._ID));
            Uri baseUri = Uri.parse("content://media/external/images/media");
            return Uri.withAppendedPath(baseUri, "" + id);
        } else {
            if (imageFile.exists()) {
                ContentValues values = new ContentValues();
                values.put(MediaStore.Images.Media.DATA, filePath);
                return context.getContentResolver().insert(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            } else {
                return null;
            }
        }
    }

    private void setPic(ImageView mImageView, String mCurrentPhotoPath) {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(bitmap);

    }

    public static File getFileFromBitmap(Context context, Bitmap bitmap, String name) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imageFile;
    }

    public static int getScreenWidth(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        return width;
    }

    public static int getScreenHeight(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int heightPixels = displayMetrics.heightPixels;
        return heightPixels;
    }

    private static Bitmap getScaledBitmap(String pathOfInputImage, int dstWidth, int dstHeight) {

        try {
            int inWidth = 0;
            int inHeight = 0;

            InputStream in = new FileInputStream(pathOfInputImage);

            // decode image size (decode metadata only, not the whole image)
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, options);
            in.close();
            in = null;

            // save width and height
            inWidth = options.outWidth;
            inHeight = options.outHeight;

            // decode full image pre-resized
            in = new FileInputStream(pathOfInputImage);
            options = new BitmapFactory.Options();
            // calc rought re-size (this is no exact resize)
            options.inSampleSize = Math.max(inWidth / dstWidth, inHeight / dstHeight);
            // decode full image
            Bitmap roughBitmap = BitmapFactory.decodeStream(in, null, options);

            // calc exact destination size
            Matrix m = new Matrix();
            RectF inRect = new RectF(0, 0, roughBitmap.getWidth(), roughBitmap.getHeight());
            RectF outRect = new RectF(0, 0, dstWidth, dstHeight);
            m.setRectToRect(inRect, outRect, Matrix.ScaleToFit.CENTER);
            float[] values = new float[9];
            m.getValues(values);

            // resize bitmap
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(roughBitmap, (int) (roughBitmap.getWidth() * values[0]), (int) (roughBitmap.getHeight() * values[4]), true);
            return resizedBitmap;
           /* // save image
            try
            {
                FileOutputStream out = new FileOutputStream(pathOfOutputImage);
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
            }
            catch (Exception e)
            {
                Log.e("Image", e.getMessage(), e);
            }*/
        } catch (IOException e) {
            Log.e("Image", e.getMessage(), e);
        }

        return null;
    }

    public static String getTotalTime(long diff) {
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        long diffDays = diff / (24 * 60 * 60 * 1000);
        String time = "";
        if (diffDays != 0) {
            time = diffDays + "d ";
        }
        if (diffHours != 0) {
            time = time + diffHours + "h ";
        }
        if (diffMinutes != 0) {
            time = time + diffMinutes + "m ";
        }
//        if (diffSeconds != 0) {
//            time = time + diffSeconds + "s";
//        }

        return time;
    }

    public void getPlacePhoto(String photoRef) {

    }

    public static Bitmap decodeSampledBitmapFromUri(Context context, Uri imageUri, int reqWidth, int reqHeight) throws FileNotFoundException {

        // Get input stream of the image
        final BitmapFactory.Options options = new BitmapFactory.Options();
        InputStream iStream = context.getContentResolver().openInputStream(imageUri);

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(iStream, null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeStream(iStream, null, options);

    }

    public void showProgressDialog(Context context, String title, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        } else {
            progressDialog.setTitle(title);
            progressDialog.setMessage(message);
            progressDialog.show();
        }
    }

    public void hideProgressDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.dismiss();
    }


}
