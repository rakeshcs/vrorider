package com.driver.vro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.driver.vro.constants.MsgConstants;
import com.driver.vro.databinding.ActivityMainBinding;
import com.driver.vro.utility.Utilities;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener {

    private ActivityMainBinding binding;
    public static Activity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        instance = this;
        setupScreen();
    }

    private void setupScreen() {
        clickListeners();
    }


    private void clickListeners() {
        binding.tvSignUpNewUser.setOnClickListener(this);
        binding.tvSignInNewUser.setOnClickListener(this);
        binding.btnLetStart.setOnClickListener(this);
        binding.scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Utilities.hideSoftKeyboard(MainActivity.this);
                return false;
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view == binding.tvSignInNewUser) {
            Intent i = new Intent(MainActivity.this, SignInActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        } else if (view == binding.btnLetStart) {
            Utilities.hideSoftKeyboard(MainActivity.this);
            if (formValidate())
                callLoginAPI();
        }

    }

    //API Call
    private void callLoginAPI() {
       /* Intent i = new Intent(MainActivity.this, SignInActivity.class);
        startActivity(i);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);*/

    }


    private boolean formValidate() {
        if (binding.etEmailId.getText().toString().length() == 0) {
            Toast.makeText(this, MsgConstants.emptyEmailAddress, Toast.LENGTH_SHORT).show();
            return false;
        } else if (binding.etMobileNumber.getText().toString().length() < 10) {
            Toast.makeText(this, MsgConstants.errorPhone, Toast.LENGTH_SHORT).show();
            return false;
        } return  true;

    }

}
