package com.driver.vro.constants;

public class ServerConstants {
    //Server URLS
    public static final String BASEPATH = "http://52.27.53.102/livzoe/api/v1/user";

    public static final String PATIENT_SIGNUP = "patient_signup";
    public static final String PATIENT_OTP_VERIFIED = "patient_otp_verified";
    public static final String PATIENT_MOBILE_EX = "patient_mobileVerification";
    public static final String DOCTOR_SIGNUP = "doctor_signup";
    public static final String DOCTOR_OTP_VERIFIED = "doctor_otp_verified";
    public static final String SEND_OTP = "send_otp";
    public static final String PATIENT_SIGNIN = "patient_signin";
    public static final String DOCTOR_SIGNIN = "doctor_signin";
    public static final String UPDATE_DEVICE_TOKEN = "update_device_token";
    public static final String UPDATE_LOCATION = "update_location";
    public static final String GET_PATIENT_DETAILS = "get_patient_details";
    public static final String EDIT_PROFILE = "edit_profile";
    public static final String CHECK_MOBILE_EXIST = "checkmobileExist";
    public static final String GET_DOCTORS_API = "get_doctors";
    public static final String FILTER_DOCTORS_API = "doctor_filter";
    public static final String SEARCH_DOCTORS_API = "doctor_search";
    public static final String FORGOT_PASSWORD_API = "forgotpassword";
    public static final String RESET_PASSWORD_API = "reset_password";
    public static final String BOOKING_APPOINTMENT_API = "booking_appointment";
    public static final String BOOKED_APPOINTMENT_BY_PATIENT = "booked_appointment_bypatient";
    public static final String BOOKED_APPOINTMENT_BY_DOCTOR = "booked_appointment_bydoctor";
    public static final String CANCEL_APPOINTMENT = "cancel_appointment";
    public static final String RESCHEDULE_APPOINTMENT = "reschedule_appointment";
    public static final String BOOKED_SLOTS_API = "booked_slots";
    public static final String APPOINTMENT_STATUS_API = "appointmentStatus";
    public static final String LIVING_HEALTHY = "getArticles";
    public static final String LIVING_EXPLORE = "getExplore";
    public static final String SAVED_DOCTOR = "get_saved_doctor";
    public static final String LIVING_EXPLORE_DETAILS = "getExploreDetail";
    public static final String LIVING_HEALTHY_DETAILS = "getArticleDetail";
    public static final String SAVED_DOCTORS = "insert_saved_doctor";
    public static final String DELETE_DOCTORS = "delete_saved_doctor";
    public static final String ADD_WALK_IN = "addWalkin";
    public static final String PATIENT_NAME = "patient_name";
    public static final String REASON = "reason";
    public static final String WALKIN_DATE = "walkin_date";
    public static final String WALKIN_TIME = "walkin_time";
    public static final String WALKIN_LISTING = "walkinListing";
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";


    //Server Keys
    public static final String METHOD = "method";
    public static final String NAME = "name";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String IS_OTP_VERIFIED = "is_otp_verified";
    public static final String PASSWORD = "password";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String USER_ID = "user_id";
    public static final String LAT = "lat";
    public static final String LONG = "long";
    public static final String EMAIL = "email";
    public static final String ADDRESS = "address";
    public static final String DATE_OF_BIRTH = "date_of_birth";
    public static final String GENDER = "gender";
    public static final String PROFILE_PICTURE = "profile_picture";
    public static final String SEARCH_STRING = "search_string";
    public static final String RANGE = "range";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String APPOINTMENT_DATE = "appointment_date";
    public static final String APPOINTMENT_TIME = "appointment_time";
    public static final String DOC_ID = "doc_id";
    public static final String PAT_ID = "pat_id";
    public static final String CLINIC_ID = "clinic_id";
    public static final String APPOINTMENT_STATUS = "appointment_status";
    public static final String STATUS = "status";
    public static final String APPOINTMENT_ID = "appointment_id";
    public static final String INSERT_FEEDBACK = "insert_feedback";
    public static final String DOCTOR_FEEDBACK = "doctor_feedback";
    public static final String RECOMMENDATION = "recommendation";
    public static final String WAITING_PERIOD = "waiting_period";
    public static final String OVERALL_EXP = "overall_experience";
    public static final String COMMENTS = "comments";
    public static final String PATIENT_ID = "patient_id";
    public static final String DOCTOR_ID = "doctor_id";
    public static final String ARTICALE_ID = "article_id";
    public static final String EXPLORE_ID = "explore_id";
    public static final String MOBILE_NO = "mobile_number";


}
