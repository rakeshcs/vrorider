package com.driver.vro.constants;

public class PrefConstants {

    public static final String PATIENT_LOGGED_IN = "patient_logged_in";
    public static final String DOCTOR_LOGGED_IN = "doctor_logged_in";
    public static final String DOCTOR_PROFILE_ADDED = "doctor_profile_added";
    public static final String DOCTOR_CLINIC_ADDED = "doctor_clinic_added";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_PROFILE_PIC = "user_profile_pic";
    public static final String IS_FIRST_TIME = "is_first_time";

    public static String KEY_DEVICE_TOKEN = "key_device_token";
}
