package com.driver.vro.constants;

public class MsgConstants {


    //Alert Msgs
    public static final String genericError = "Something went wrong. Please try again";

    //Alert Msgs
    public static final String emptyEmail = "Please enter email id";
    public static final String emptyEmailPhone = "Please enter email id or phone number";
    public static final String emptyEmailPhone1 = "Please enter valid phone number and password";
    public static final String errorEmail = "Please enter a valid email id";
    public static final String emptyPhone = "Please enter phone number";
    public static final String errorPhone = "Please enter a valid phone number";
    public static final String errorPhoneExists = "Mobile number already exists.";
    public static final String emptyPassword = "Please enter password";
    public static final String errorPassword = "Password must be at least 6 characters long";
    public static final String emptyNewPassword = "Please enter new password";
    public static final String emptyOldPassword = "Please enter old password";
    public static final String errorNewPassword = "New password must be at least 6 characters long";
    public static final String errorOldPassword = "Old password must be 6 characters long";
    public static final String emptyConfirmPassword = "Please confirm password";
    public static final String errorConfirmPassword = "Passwords do not match";
    public static final String errorPasswordLength = "Passwords must be of minimum 6 characters";
    public static final String errorDOB = "Please enter your date of birth";
    public static final String networkError = "Please check your internet connection";
    public static final String error = "Something went wrong. Please try again";
    public static final String errorProfilePic = "Please upload your profile picture";
    public static final String errorName = "Please enter your full name";
    public static final String feedback = "Please enter your feedback";
    public static final String errorAboutMe = "Please add some information about yourself";
    public static final String successPassReset = "Password reset successfully";
    public static final String fbEmailError = "No Email found using Facebook. Please use other mode to Login/Register";
    public static final String emptyCity = "Please enter your address";
    public static final String emptyLocality = "Please enter your address";
    public static final String emptyAddress = "Please enter your address";
    public static final String emptyDob = "Please enter your date of birth";
    public static final String emptyWalkIndate = "Please add your walk-in date";
    public static final String emptyWalkInTime = "Please add your walk-in time";

    public static final String errorTerms = "Please accept terms & conditions";
    public static final String errorOTP = "Incorrect OTP. Please try again";
    public static final String errorEmptyOTP = "Please enter OTP";
    public static final String successSendOtp = "OTP sent successfully";
    public static final String errorSocialLogin = "Error while login. Please try again!";
    public static final String dupliacatePass = "Old password and new password can not be same";
    public static final String errorUploadPics = "Please upload at least one picture";
    public static final String passwordChangedSuccess = "Password reset successful. Please login with new password.";


    //Doctor- Complete Profile screen
    public static final String emptyFullName = "Please enter full name";
    public static final String emptyEmailAddress = "Please enter email address";
    public static final String emptyFullAddress = "Please enter full address";
    public static final String emptySpeciality = "Please enter speciality";
    public static final String emptyDegreesQualifications = "Please enter Degrees/Qualifications";
    public static final String emptyFees = "Please enter fees";
    public static final String emptyShortDescription = "Please enter short description";
    public static final String emptyMedicalLicense = "Upload your medical license";

    //Add Clinic screen
    public static final String emptyName = "Please enter your name";
    public static final String emptyPatientName = "Please enter your patient name";
    public static final String emptyClinicAddress = "Please enter clinic address";
    public static final String emptyClinicType = "Please select clinic type";
    public static final String emptyTimings = "Please select clinic atleast one timings";
    public static final String emptyEmoji = "What was your appointment experience";

    //Add Reminder screen
    public static final String emptyReminderTitle = "Please enter reminder title";
    public static final String emptyReminderDesc = "Please enter reminder description";
    public static final String emptyReminderTimings = "Please choose reminder time";
    public static final String emptyReminderDate = "Please choose reminder date";
    public static final String emptyReminderDay = "Please choose reminder days";
    public static final String reminderAddedSuccessMsg = "Reminder added succesfully";
    public static final String reminderUpdatedSuccessMsg = "Reminder updated succesfully";
    public static final String feedbacksubmitSuccessMsg = "Feedback submit succesfully";
    public static final String errorGender = "Please select your gender";
    public static final String errorReaptDay = "Please select your day";
    public static final String successUpdateProfile = "Profile ";

    //Update Clinic screen
    public static final String clinicDeletedSuccess = "Clinic has been deleted successfully";
    public static final String clinicCountError = "You can add upto 3 clinics.";

    public static String errorSlot = "Please select a slot";
}
