package com.driver.vro.constants;

public class IntentConstants {
    public static final String MODE = "mode";
    public static final String OTP = "otp";
    public static final String COUNTRY_CODE ="country_code" ;
    public static final String USER_ID = "userId";
    public static final String USER_PROFILE_PIC = "user_profile_pic";
    public static final String MOBILE_NUMBER = "mobile_number";
    public static final String PASSWORD = "password";


    public static final String NAME = "name";
    public static final String PATIENT_PROFILE = "patient_profile";
    public static String TIMINGS = "timings";
    public  static  String SOCKET_URL = "http://52.27.53.102:3123";
}
